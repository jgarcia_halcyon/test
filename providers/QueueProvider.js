'use strict'

const { ServiceProvider } = require('@adonisjs/fold');

const Queue = require("bull");

class QueueProvider extends ServiceProvider {

  register () {
    this.app.singleton("Providers/QueueProvider", app => {
      return new Queue("App", "redis://127.0.0.1:6379", {
        prefix: "hlcynnjs101"
      });
    });

    this.app.alias("Providers/QueueProvider", "Queue");
  }

  boot () {
    // Start job processor.
    const Queue = this.app.use("Queue");
    const Mail = this.app.use("Mail");

    console.log("Queue listener started");

    Queue.process(async (job, done) => {
      const { name, payload } = job.data;

      if (name === "EmailQueue") {
        const { to, message, from } = payload;

        try {
          await Mail.raw(message, message => {
            message.from(from);
            message.to(to);
          });

          console.log('Email sent to ${to}');
          done();
        } catch (error) {
          console.log(error);
          throw error;
        }
      }
    });
  }
}

module.exports = QueueProvider

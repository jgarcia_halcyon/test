'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
const Env = use('Env');
// use article model
const article = use('App/Models/Article');

// use mail
const Mail = use('Mail');

// use queue
const Queue = use('Queue')

const Config = use("Config");
const swaggerJSDoc = use('swagger-jsdoc');

Route.get("/", "ArticleController.index");
// Route.on('Article')

// Articles
Route.get("/articles", "ArticleController.index").middleware("auth");
Route.post("/articles", "ArticleController.store").middleware("auth");

Route.get("/articles/:slug", "ArticleController.show");
Route.put("/articles/:slug", "ArticleController.update").middleware("auth");
Route.get("/articles/edit/:slug", "ArticleController.edit");
Route.delete("/articles/:slug", "ArticleController.destroy").middleware("auth");

// Auth
Route.post("/register", "AuthController.register");
Route.post("/login", "AuthController.login");
Route.get("/user", "AuthController.user").middleware("auth");

// Socket.io
Route.get('/chat', ({
	view
}) => {
  	return view.render('chat')
});

// Mail through route
// Route.get('test-email', async () => {
// 	await Mail.raw('<h1 style="color: red;">HELLO!</h1>', (message) => {
// 		message.from('from@gmail.com')
// 		message.to('to@gmail.com')
// 	})

// 	return 'test-email'
// });

// Mail through controller
Route.get('/test-mail', 'MailController.send');

// Queue through route
// Route.get('test-queue', async ({ request, response }) => {

// 	await Queue.add({ name: 'EmailQueue', payload: { to: 'test@gmail.com', message: 'TEST' } }, { delay: 10 * 1000 })

// 	return 'test-queue'
// })

// Queue through controller
Route.get('test-queue', 'MailController.queue');

// Route.get('/api-specification', () => {
//   return `
// openapi: "3.0.0"
// info:
//   version: 1.0.0
//   title: API Documentation
//   license:
//     name: MIT
// servers:
//   - url: ${Env.get('APP_URL')}
// components:
//   schemas:
//     Article:
//       type: object
//       properties:
//         title:
//           type: string
//         slug:
//           type: string
//         body:
//           type: string
// paths:
//   /articles:
//     get:
//       tags:
//         - Articles
//       summary: List all articles
//       operationId: listArticles
//       responses:
//         200:
//           description: Article list
//           content:
//             application/json:
//               schema:
//                 type: object
//                 properties:
//                   message:
//                     type: string
//                   data:
//                     type: array
//                     items:
//                       $ref: "#/components/schemas/Article"
//     post:
//       summary: Create an article
//       operationId: createArticle
//       tags:
//         - Articles
//       requestBody:
//         content:
//           multipart/form-data:
//             schema:
//               type: object
//               required:
//                 - title
//                 - body
//               properties:
//                 title:
//                   type: string
//                   default: Default Title
//                 body:
//                   type: string
//       responses:
//         201:
//           description: Article created
//           content:
//             application/json:
//               schema:
//                 $ref: "#/components/schemas/Article"
// `
// })

// Swagger
Route.get('/api-swagger', ({
  	view
}) => {
  	return view.render('swagger', {
    	specUrl: '/api-specification'
  	})
})

Route.get('/api-specification', async () => {
  // https://swagger.io/docs/specification/about/
	const swaggerDefinition = {
		openapi: '3.0.0',
		info: {
			title: Config.get('app.name'),
			version: '1.0.0'
		},
		servers: [{
			url: Env.get('APP_URL')
		}],
		// basePath: '/articles',
		// security: [{ bearerAuth: [] }],
		// schemes: Env.get('NODE_ENV') === 'development' ? ['https'] : ['http'],
		components: {
			// securitySchemes: {
			//     bearerAuth: {
			//         type: 'http',
			//         scheme: 'bearer',
			//         bearerFormat: 'JWT'
			//     }
			// },
			schemas: {
				"Article": {
					"type": "object",
					"properties": {
						"title": {
							"type": "string",
						},
						"slug": {
							"type": "string",
						},
						"body": {
							"type": "string",
						}
					}
				}
			}
		},
		paths: {
			"/articles": {
				"get": {
					"tags": [
						"Articles"
					],
					"summary": "List all articles",
					"operationId": "listArticles",
					"responses": {
						"200": {
							"description": "Article list",
							"content": {
								"application/json": {
									"schema": {
										"type": "object",
										"properties": {
											"message": {
												"type": "string"
											},
											"data": {
												"type": "array",
												"items": {
													"$ref": "#/components/schemas/Article"
												}
											}
										}
									}
								}
							}
						}
					}
				},
				"post": {
					"summary": "Create an article",
					"operationId": "createArticle",
					"tags": [
						"Articles"
					],
					"requestBody": {
						"content": {
							"multipart/form-data": {
								"schema": {
									"type": "object",
									"required": [
										"title",
										"body"
									],
									"properties": {
										"title": {
											"type": "string",
											"default": "Default Title"
										},
										"body": {
											"type": "string"
										}
									}
								}
							}
						}
					},
					"responses": {
						"201": {
							"description": "Article created",
							"content": {
								"application/json": {
									"schema": {
										"$ref": "#/components/schemas/Article"
									}
								}
							}
						}
					}
				}
			}
		}
	};

  	const options = {
    	definition: swaggerDefinition,
    	apis: ['./start/**/**.js', './start/**/**.yaml']
  	};

  	return swaggerJSDoc(options);
});

Route.get('/api-blog', ({
	view
}) => {
	return view.render('swagger', {
	  specUrl: '/api-blogspec'
	})
});

Route.get('/api-blogspec', 'SwaggerController.blog');

// Route.get('/api-specification', async () => {
//     // https://swagger.io/docs/specification/about/
//     const swaggerDefinition = {
//         openapi: '3.0.0',
//         info: {
//             title: Config.get('app.name'),
//             version: '1.0.0'
//         },
//         servers: [{
//             url: Env.get('APP_URL')
//         }],
//         basePath: '/',
//         security: [{
//             bearerAuth: []
//         }],
//         schemes: Env.get('NODE_ENV') === 'development' ? ['https'] : ['http'],
//         components: {
//             securitySchemes: {
//                 bearerAuth: {
//                     type: 'http',
//                     scheme: 'bearer',
//                     bearerFormat: 'JWT'
//                 }
//             }
//         },
//     };

//     const options = {
//         definition: swaggerDefinition,
//         apis: ['./start/**/**.js', './start/**/**.yaml']
//     };

//     return swaggerJSDoc(options);
// });

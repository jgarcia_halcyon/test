'use strict'

const mongoose = use("Providers/Mongoose");

const Article = mongoose.model('Article', {
    title: { type: String, unique: true },
    body: { type: String },
    slug: { type: String, unique: true}
});

module.exports = Article

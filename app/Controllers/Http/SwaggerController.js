'use strict'

const swaggerJSDoc = use('swagger-jsdoc');
const Env = use('Env');
const Config = use("Config");

class SwaggerController {

    async blog(){
        const swaggerDefinition = {
            openapi: '3.0.0',
            info: {
                title: Config.get('app.name'),
                version: '1.0.0'
            },
            servers: [{
                url: Env.get('APP_URL')
            }],
            // basePath: '/',
            // security: [{ bearerAuth: [] }],
            schemes: Env.get('NODE_ENV') === 'development' ? ['https'] : ['http'],
            components: {
                securitySchemes: {
                    bearerAuth: {
                        type: 'http',
                        scheme: 'bearer',
                        bearerFormat: 'JWT'
                    }
                },
                schemas: {
                    "Login": {
                        "type": "object",
                        "properties": {
                            // "_id": {
                            //     "type": "string",
                            // },
                            "email": {
                                "type": "string",
                            },
                            "password": {
                                "type": "string",
                            }
                        }
                    },
                    "Register": {
                        "type": "object",
                        "properties": {
                            "email": {
                                "type": "string",
                            },
                            "firstName": {
                                "type": "string",
                            },
                            "lastName": {
                                "type": "string",
                            },
                            "password": {
                                "type": "string",
                            }
                        }
                    },
                    "Article-List": {
                        "type": "object",
                        "properties": {
                            "title": {
                                "type": "string",
                            },
                            "slug": {
                                "type": "string",
                            },
                            "body": {
                                "type": "string",
                            }
                        }
                    }
                }
            },
            paths: {
                "/register": {
                    "post": {
                        "summary": "Register",
                        "operationId": "registerBlog",
                        "tags": [
                            "Login/Register"
                        ],
                        "requestBody": {
                            "content": {
                                "multipart/form-data": {
                                    "schema": {
                                        "type": "object",
                                        "required": [
                                            "email",
                                            "firstName",
                                            "lastName",
                                            "password",
                                        ],
                                        "properties": {
                                            "email": {
                                                "type": "string",
                                                "default": "sample@gmail.com"
                                            },
                                            "firstName": {
                                                "type": "string"
                                            },
                                            "lastName": {
                                                "type": "string"
                                            },
                                            "password": {
                                                "type": "string"
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "responses": {
                            "201": {
                                "description": "Successful Registered",
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "$ref": "#/components/schemas/Register"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "/login": {
                    "post": {
                        "summary": "Login",
                        "operationId": "loginBlog",
                        "tags": [
                            "Login/Register"
                        ],
                        "requestBody": {
                            "content": {
                                "multipart/form-data": {
                                    "schema": {
                                        "type": "object",
                                        "required": [
                                            "email",
                                            "password"
                                        ],
                                        "properties": {
                                            "email": {
                                                "type": "string",
                                                "default": "sample@gmail.com"
                                            },
                                            "password": {
                                                "type": "string"
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "responses": {
                            "201": {
                                "description": "Successful login",
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "$ref": "#/components/schemas/Login"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "/articles": {
                    "get": {
                        "security": [{ "bearerAuth": [] }],
                        "tags": [
                            "Articles"
                        ],
                        "summary": "List all articles",
                        "operationId": "listArticles",
                        "responses": {
                            "200": {
                                "description": "Article list",
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "type": "object",
                                            "properties": {
                                                "message": {
                                                    "type": "string"
                                                },
                                                "data": {
                                                    "type": "array",
                                                    "items": {
                                                        "$ref": "#/components/schemas/Article-List"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "post": {
                        "security": [{ "bearerAuth": [] }],
                        "summary": "Create an article",
                        "operationId": "createArticle",
                        "tags": [
                            "Articles"
                        ],
                        "requestBody": {
                            "content": {
                                "multipart/form-data": {
                                    "schema": {
                                        "type": "object",
                                        "required": [
                                            "title",
                                            "body"
                                        ],
                                        "properties": {
                                            "title": {
                                                "type": "string",
                                                "default": "Default Title"
                                            },
                                            "body": {
                                                "type": "string"
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "responses": {
                            "201": {
                                "description": "Article created",
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "$ref": "#/components/schemas/Article-List"
                                        }
                                    }
                                }
                            }
                        }
                    },
                },
                "/articles/{slug}": {
                    "put": {
                        "security": [{ "bearerAuth": [] }],
                        "summary": "Update an article",
                        "operationId": "updateArticle",
                        "tags": [
                            "Articles"
                        ],
                        "parameters": [
                            {
                                "name": "slug",
                                "in": "path",
                                "description": "Article update base on slug",
                                // "required": true,
                                "type": "string",
                            }
                        ],
                        "requestBody": {
                            "content": {
                                "multipart/form-data": {
                                    "schema": {
                                        "type": "object",
                                        "required": [
                                            "title",
                                            "body"
                                        ],
                                        "properties": {
                                            "title": {
                                                "type": "string",
                                            },
                                            "body": {
                                                "type": "string"
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "responses": {
                            "201": {
                                "description": "Article Updated",
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "$ref": "#/components/schemas/Article-List"
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "delete": {
                        "security": [{ "bearerAuth": [] }],
                        "summary": "Delete an article",
                        "operationId": "deleteArticle",
                        "tags": [
                            "Articles"
                        ],
                        "parameters": [
                            {
                                "name": "slug",
                                "in": "path",
                                "description": "Article slug to delete",
                                // "required": true,
                                "type": "string",
                            }
                        ],
                        "responses": {
                            "201": {
                                "description": "Article Deleted",
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "$ref": "#/components/schemas/Article-List"
                                        }
                                    }
                                }
                            }
                        }
                    },
                }
            }
        };
    
        const options = {
            definition: swaggerDefinition,
            apis: ['./start/**/**.js', './start/**/**.yaml']
        };

        return swaggerJSDoc(options);
    }
}

module.exports = SwaggerController

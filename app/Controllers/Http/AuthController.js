'use strict'

const Jwt = use("Jwt");
const Hash = use("Hash");
const User = use("App/Models/User");
const Mail = use("Mail");
const Queue = use("Queue");

class AuthController {
    
    // Login AUTH
    async login({ request, response }) {
        const { email, password } = request.all();
    
        const user = await User.findOne(
            { email, deletedAt: null },
            "+password"
        ).lean();
    
        if (!user) {
            return response
                .status(422)
                .json({ message: "Invalid Username or Password" });
        }
    
        const isSame = await Hash.verify(password, user.password);
    
        if (!isSame) {
            return response
                .status(422)
                .json({ message: "Invalid Username or Password" });
        }
        
        // mailing
        if(user && isSame){
            // console.log('working on');
            try{
                // const mailing = await Mail.raw('Message Send Successfully', (message) => {
                //     message.from(user.email)
                //     message.to('to@gmail.com')
                // });
                const queueing = await Queue.add({
                    name: 'EmailQueue',
                    payload: {
                        to: 'test@gmail.com',
                        from: user.email,
                        message: 'Login successfully:' + '\n\nEmail: ' + user.email
                    },
                },
                {
                    delay: 10 * 1000
                })
            }catch(error){
                throw error
            }
        }

        const names = user.name;
        
        const token = Jwt.generate({
            user: String(user._id),
            email: String(user.email),
            // firstname: String(user.name.firstName),
            // lastname: String(user.name.lastName),
            firstname: String(names.firstName),
            lastName: String(names.lastName),
            password: String(user.password)
        });

        console.log(user);
    
        return response.ok({ message: "Login Successful", data: token });
    }
    
    // Register
    async register({ request, response }) {
        const { email, firstName, lastName, password } = request.all();

        // const reg = request.post();
        // console.log(firstName);
        try {
            const user = await User.create({
                email: email,
                name: { firstName, lastName },
                // firstName: firstName,
                // lastName: lastName,
                password: await Hash.make(password)
            });

            // mailing
            if(user){
                try{
                    const names = user.name;
                    const queueing = await Queue.add({
                        name: 'EmailQueue',
                        payload: {
                            to: 'test@gmail.com',
                            from: user.email,
                            message: 'Successfully Registered' + '\n\nEmail:' + user.email + '\nFirst Name:' + names.firstName + '\nLast Name:' + names.lastName
                        },
                    },
                    {
                        delay: 10 * 1000
                    });
                }catch(error){
                    throw error
                }
            }
            
            console.log(user);
            return response
                .status(201)
                .json({ data: user, message: "Registration Successful" });
        }catch (error) {
            console.log(error);
            return response.status(500).json({ message: "Something went wrong" });
        }
    }
    
    // List of users
    async user({ response, auth }) {
        // console.log(auth)
        return response.ok({ data: auth.user });
    }
}

module.exports = AuthController

'use strict'

const Mail = use('Mail');
const Queue = use('Queue');

class MailController {
    
    // mail send
    async send({response}){

        try{
            const mailing = await Mail.raw('Message Send Successfully', (message) => {
                message.from('from@gmail.com')
                message.to('to@gmail.com')
            })

            return response.json({message: mailing})
        }catch(error){
            throw error
        }
    }
    
    // on queue
    async queue({request, response}){

        try{
            const queueing = await Queue.add({
                name: 'EmailQueue',
                payload: {
                    to: 'test@gmail.com',
                    from: 'from@gmail.com',
                    message: 'TEST Queue'
                },
            },
            {
                delay: 10 * 1000
            })

            return response.json({message: queueing})
        }catch(error){
            throw error
        }
    }
}

module.exports = MailController

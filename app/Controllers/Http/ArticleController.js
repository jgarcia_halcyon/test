'use strict'

// const articles = [
//     {
//       title: "Article 1",
//       slug: "article-1",
//       body: "Article 1 body"
//     }
//   ];

// const { validate } = use('Validator');

const Articles = use('App/Models/Article');

const slug = require('slug');

class ArticleController{

    // List of articles data
    async index({ response }) {
      const article = await Articles.find({});
      return response.json({ message: "success", data: article });
    }
    
    // List of specific articles data
    async show({ params, response }) {
      
      const { slug } = params;
      // const article = articles.find(item => item.slug === slug);
      const article = await Articles.findOne({
        slug: slug
      });
  
      return response.json({ message: "success", data: article });
    }
    
    // Create Field
    async store({ request, response }) {
      
      // const validation = await validate(request.all(), {
      //   title: 'required',
      //   body: 'required'
      // })

      // const { a_title, a_body, a_slug } = request.post();

      const article = request.post();

      try {
        await Articles.create({
          title: article.title,
          body: article.body,
          slug: slug(article.title),
          
        });
      }catch (error) {
        throw error
      }
    
      return response.created({ message: "created", data: article });
    }
    
    // Update Field
    async update({ request, response, params }) {

      const article = request.all();
      const { slug } = params;

      try{
        await Articles.updateOne(
          { slug: slug },
          { $set: { 'title': article.title, 'body': article.body }}
        )
      }catch(error){
        throw error
      }
  
      return response.json({ message: "updated", data: {'updated': article, 'parameter': params} });
    }
    
    // Delete Field
    async destroy({ params, response }) {

      const { slug } = params;

      try{
        await Articles.deleteOne({
          slug: slug,
        })
      }catch(error){
        throw error
      }
  
      return response.json({ message: "deleted" });
    }
}

module.exports = ArticleController
